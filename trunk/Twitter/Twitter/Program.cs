﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitter
{
   static class Database
   {
       public static papmelindadbEntities db = new papmelindadbEntities();
   }
    class Program
    {

        static void Main(string[] args)
        {
            TraceListener TL = new TextWriterTraceListener(@"C:\Temp\ObserverLog.txt");
            TraceListener TL2 = new ConsoleTraceListener();
            Trace.Listeners.Add(TL);
            Trace.Listeners.Add(TL2);
            Trace.TraceInformation("Logolás elkezdve");
            Trace.Flush();

            // Ezt a változót állíthatjuk tesztelés céljából
            // Ha true, akkor mentünk az adatbázisba is, 
            // Ha false, akkor nem használjuk az adatbázist.
            bool adatbazisbamentes = false;


            Celeb aktualisCeleb = new Celeb("Will Smith", adatbazisbamentes);


            Follower Koveto1 = new Follower("Kiss Pista", 2, adatbazisbamentes);


            Follower Koveto2 = new Follower("Teszt Elek", 1, adatbazisbamentes);
            Follower Koveto3 = new Follower("Péter Pál", 0, adatbazisbamentes);


            Koveto1.startFollowing(aktualisCeleb);
            Koveto2.startFollowing(aktualisCeleb);
            Koveto3.startFollowing(aktualisCeleb);
            aktualisCeleb.Posting("Első poszt");
            aktualisCeleb.Posting("Második poszt");         


            Trace.TraceInformation("Logolás befejezve");
            Trace.Flush();

            //Törlés adatbázisból
           /* post lastpost = Database.db.post.First();
            Database.db.post.Remove(lastpost);
            Database.db.SaveChanges();
            * */

            // kapcsolt adatok lekérése adatbázisból

            var postlist = from p in Database.db.post
                           join u in Database.db.users
                           on p.personId equals u.id
                           select new Postlist()
                           {
                               UserName = u.Name,
                               Message = p.Message,
                               Timestamp = p.TimeStamp
                           };

            var postListselected = from p in Database.db.post
                                   where p.personId == 40
                                   select p;

            var postListselected2 = Database.db.post.Select(p => p.personId == 40);

            List<post> plist = new List<post>(postListselected.ToList());

            List<Postlist> mylist = new List<Postlist>(postlist);

            foreach (var item in mylist)
            {
                Console.WriteLine(item.Timestamp +"\t" + item.UserName + ": " + item.Message);
            }

            Console.ReadLine();
        }
    }
    public interface IObservable
    {
        /// <summary>
        /// Felíratkozás
        /// </summary>
        /// <param name="o">Felíratkozni kívánt observer</param>
        void Subscribe(IObserver o);
        /// <summary>
        /// Leíratkozás
        /// </summary>
        /// <param name="o">Leíratkozni kívánt observer</param>
        void UnSubscribe(IObserver o);
        /// <summary>
        /// Kiértesíti az összes felíratkozott observert
        /// </summary>
        void Notify();
    }
    public interface IObserver
    {
        /// <summary>
        /// Megkapja a megfigyelt objektumtumoktól a postot.
        /// </summary>
        /// <param name="post">Az üzenet</param>
        void NewPost(string post);
        void startFollowing(IObservable o);
        void followend();

    }

    public class Celeb : IObservable
    {
        List<IObserver> followers;

        public List<IObserver> Followers
        {
            get { return followers; }
        }

        string post;
        string name;
        int id;

        /// <summary>
        /// A celeb id-ja. Ha a celeb nincs mentve az adatbázisba, 
        /// akkor az értéke = 0.
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public users ConverToUser()
        {            
            users newUser = new users();            
            newUser.id = this.id;
            newUser.Name = this.name;
            newUser.NumOfFallowers = this.followers.Count;
            return newUser;
        }

        /// <summary>
        /// Az osztály konstruktora, mely létrehoz egy 
        /// Celeb egyedet. Az adatbázisba viszont csak 
        /// akkor ment, ha az erre vonatkozó paraméter igaz.
        /// </summary>
        /// <param name="name">A celeb neve</param>
        /// <param name="saveToDatabase">Egy igaz/hamis érték, mely megadja, 
        /// hogy kell-e menteni az elemet az adatbázisba.</param>
        public Celeb(string name, bool saveToDatabase)
        {
            this.name = name;
            followers = new List<IObserver>();

            if (saveToDatabase)
            {
                users aktuser = this.ConverToUser();
                Database.db.users.Add(aktuser);
                Database.db.SaveChanges();
                id = aktuser.id;
            }

            Trace.TraceInformation("Celeb létrehozva "+name);
            Trace.Flush();
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public void Subscribe(IObserver o)
        {
            if (!followers.Contains(o))
            {
                followers.Add(o);
                // Csak akkor módosítjuk az adatbázisbeli user-t
                // ha már mentve van.
                if (this.Id != 0)
                {
                    users u = Database.db.users.Find(this.Id);
                    u.NumOfFallowers++;
                    Database.db.SaveChanges();
                }

                Trace.TraceInformation("Feliratkozás sikeres");
                Trace.Flush();
            }
        }

        public void UnSubscribe(IObserver o)
        {
            if (followers.Contains(o))
            {
                followers.Remove(o);
                // Csak akkor módosítjuk az adatbázisbeli user-t
                // ha már mentve van.
                if (this.Id != 0)
                {
                    users u = Database.db.users.Find(id);
                    u.NumOfFallowers--;
                    Database.db.SaveChanges();
                }
                Trace.TraceInformation("Leiratkozás sikeres");
                Trace.Flush();
            }
        }

        public void Notify()
        {
            for (int i = 0; i < followers.Count; i++)
            {
                followers[i].NewPost(post);
            }
        }
        public void Posting(string newpost)
        {
            post = newpost;
            Console.WriteLine(post);
            // Csak akkor mentjük a post-ot az adatbázisba, ha 
            // az aktuális user is mentve van.
            if (this.Id != 0)
            {
                post p = new post();
                p.Message = post;
                p.personId = this.id;
                p.TimeStamp = System.DateTime.Now;
                Database.db.post.Add(p);
                Database.db.SaveChanges();
            }
            
            Trace.TraceInformation(name +": "+ post);
            Trace.Flush();
            Notify();
        }
    }
    public class Follower:IObserver
    {
        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        string lastpost;
        int id;

        /// <summary>
        /// A celeb id-ja. Ha a celeb nincs mentve az adatbázisba, 
        /// akkor az értéke = 0.
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        IObservable star;

        /*
         * 0-boldog
         * 1-normál
         * 2-szomorú
         */
        int mood;
        public void NewPost(string post)
        {
            lastpost = post;
            if (mood==1)
            {
                Repost();
                Trace.TraceInformation(name + ": repost " + post);
                Trace.Flush();
            }
            else if (mood==0)
            {
                Like();
                Trace.TraceInformation(name + ": Like " + post);
                Trace.Flush();
            }    
            
        }

        public users ConverToUser()
        {
            users newUser = new users();
            newUser.id = this.id;
            newUser.Name = this.name;
            newUser.NumOfFallowers = 0;
            return newUser;
        }
        /// <summary>
        /// Létrehozunk egy követőt
        /// </summary>
        /// <param name="name">A követő neve</param>
        /// <param name="mood">A követő hangulata(0-boldog, 1-normál, 2-szomorú)</param>
        /// <param name="saveToDatabase">Egy igaz/hamis érték, mely megadja, 
        /// hogy kell-e menteni az elemet az adatbázisba.</param>
        public Follower(string name, int mood, bool saveToDatabase)
        {
            this.name = name;
            this.mood = mood;
            if (saveToDatabase)
            {
                users aktuser = this.ConverToUser();
                Database.db.users.Add(aktuser);
                Database.db.SaveChanges();
                id = aktuser.id;
            }
           
            Trace.TraceInformation("Követő létrehozva " +name);
            Trace.Flush();
        }

        private void Like()
        {
            string myNewPost = "Likeoltam: " + lastpost;
            Console.WriteLine(myNewPost);
            // Csak akkor mentjük a post-ot az adatbázisba, ha 
            // az aktuális user is mentve van.
            if (this.Id != 0)
            {
                post p = new post();
                p.Message = myNewPost;
                p.personId = this.id;
                p.TimeStamp = System.DateTime.Now;
                Database.db.post.Add(p);
                Database.db.SaveChanges();
            }

            Trace.TraceInformation(name + ": " + myNewPost);
            Trace.Flush();
        }


        private void Repost()
        {
            string myNewPost = "Repostoltam: " + lastpost;
            Console.WriteLine(myNewPost);
            // Csak akkor mentjük a post-ot az adatbázisba, ha 
            // az aktuális user is mentve van.
            if (this.Id != 0)
            {
                post p = new post();
                p.Message = myNewPost;
                p.personId = this.id;
                p.TimeStamp = System.DateTime.Now;
                Database.db.post.Add(p);
                Database.db.SaveChanges();
            }

            Trace.TraceInformation(name + ": " + myNewPost);
            Trace.Flush();
        }
        public void startFollowing(IObservable o)
        {
            o.Subscribe(this);
            star = o;
        }
        public void followend()
        {
            star.UnSubscribe(this);
            star = null;
        }
    }

    public class Postlist
    {
        string userName;
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        string message;
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        DateTime timestamp;
        public DateTime Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }

        public Postlist(string _name, string _post, DateTime _timestamp)
        {
            this.userName = _name;
            this.message = _post;
            this.timestamp = _timestamp;
        }

        public Postlist() { }
    }
}
