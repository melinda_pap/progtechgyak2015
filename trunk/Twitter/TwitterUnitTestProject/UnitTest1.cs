﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Twitter;

namespace TwitterUnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CelebCreationTestMethod()
        {
            string name = "joly";
            // Nem használjuk az adatbázist, tehát a második paraméter = false
            Twitter.Celeb actC = new Twitter.Celeb(name, false);

            // Ha a név nem jól állítódik be, elbukik a teszt
            Assert.AreEqual(actC.Name, name);

            // Ha az újonnan létrehozott celebnek már van követője (lehetetlen),
            // akkor szinténelbukik a teszt.
            Assert.AreEqual(actC.Followers.Count, 0);
        }

        [TestMethod]
        public void FollowercreationTestmethod()
        {
            string name = "Követő Kati";
            int mood = 1;
            // Nem használjuk az adatbázist, tehát a második paraméter = false
            Twitter.Follower actC = new Twitter.Follower(name, mood,false);
            // Ha a név nem jól állítódik be, elbukik a teszt
            Assert.AreEqual(actC.Name, name);
        }

        [TestMethod]
        public void SubscriptionTestMethod()
        {
            // Nem használjuk az adatbázist, tehát a második paraméter = false

            Twitter.Celeb actC = new Twitter.Celeb("Celeb Teszt", false);
            // A követők száma az új követő hozzáadása előtt.
            int numoffollowers = actC.Followers.Count;

            // Létrehozzuk a követőt
            Twitter.Follower actF = new Twitter.Follower("Követő teszt", 1, false);      

            // az új követő feliratkozik
            actC.Subscribe(actF);
            // Arra számítunk, hogy a követők száma eggyel emelkedik
            int expectedNumoffollowers = numoffollowers + 1;

            // A teszt elbukik, ha a követők száma nem nőtt eggyel.
            Assert.AreEqual(expectedNumoffollowers, actC.Followers.Count);

            // A teszt elbukik, ha nem igaz az, hogy a sztár követőinek listájában 
            // benne van az új követő.
            Assert.IsTrue(actC.Followers.Contains(actF));
        }
    }
}
